﻿using Mini_Erp.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mini_Erp
{
    public partial class FecharCaixa : Form
    {
        public FecharCaixa()
        {
            InitializeComponent();

            userTextBox.Text = Authentification.name;
            aberturaValorTextBox.Text = StaticCashier.openingValue.ToString();
            horarioAberturaTextBox.Text = StaticCashier.openingDateTime.ToString();
            valorFechamentoTextBox.Text = StaticCashier.shutdownValue.ToString();
            horarioFechamentoTextBox.Text = DateTime.Now.ToString();
            quantidadeVendasTextBox.Text = StaticCashier.venda.ToString();
            quantidadeProdutosTextBox.Text = StaticCashier.productsQuatitySelledInDay.ToString();
        }

        private void sairLabel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
