﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini_Erp.Enuns
{
    class Roles
    {
        public const string ADMIN = "1";
        public const string SELLER = "2";
        public const string STOCKIST = "3";
    }
}
