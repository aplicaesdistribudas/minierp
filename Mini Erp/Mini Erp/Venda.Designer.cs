﻿namespace Mini_Erp
{
    partial class Venda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addProdutoButton = new System.Windows.Forms.Button();
            this.fecharCompraButton = new System.Windows.Forms.Button();
            this.listaProdutosDataGridView = new System.Windows.Forms.DataGridView();
            this.Código = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descrição = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorTotalProduto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantidadeLabel = new System.Windows.Forms.Label();
            this.quantidadeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.codigoProdutoTextBox = new System.Windows.Forms.TextBox();
            this.codigoProdutoLabel = new System.Windows.Forms.Label();
            this.valorTotalDinamicoLabel = new System.Windows.Forms.Label();
            this.valorTotalLabel = new System.Windows.Forms.Label();
            this.operadorNameLabel = new System.Windows.Forms.Label();
            this.voltarButton = new System.Windows.Forms.Button();
            this.produtoImagemPictureBox = new System.Windows.Forms.PictureBox();
            this.fecharCaixaButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.listaProdutosDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.produtoImagemPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // addProdutoButton
            // 
            this.addProdutoButton.Location = new System.Drawing.Point(484, 106);
            this.addProdutoButton.Name = "addProdutoButton";
            this.addProdutoButton.Size = new System.Drawing.Size(107, 47);
            this.addProdutoButton.TabIndex = 25;
            this.addProdutoButton.Text = "Adicionar";
            this.addProdutoButton.UseVisualStyleBackColor = true;
            this.addProdutoButton.Click += new System.EventHandler(this.addProdutoButton_Click);
            // 
            // fecharCompraButton
            // 
            this.fecharCompraButton.Location = new System.Drawing.Point(656, 429);
            this.fecharCompraButton.Name = "fecharCompraButton";
            this.fecharCompraButton.Size = new System.Drawing.Size(187, 54);
            this.fecharCompraButton.TabIndex = 23;
            this.fecharCompraButton.Text = "Fechar Compra";
            this.fecharCompraButton.UseVisualStyleBackColor = true;
            this.fecharCompraButton.Click += new System.EventHandler(this.fecharCompraButton_Click);
            // 
            // listaProdutosDataGridView
            // 
            this.listaProdutosDataGridView.AllowUserToAddRows = false;
            this.listaProdutosDataGridView.AllowUserToDeleteRows = false;
            this.listaProdutosDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaProdutosDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Código,
            this.Descrição,
            this.Quantidade,
            this.valor,
            this.valorTotalProduto});
            this.listaProdutosDataGridView.Location = new System.Drawing.Point(22, 184);
            this.listaProdutosDataGridView.Name = "listaProdutosDataGridView";
            this.listaProdutosDataGridView.ReadOnly = true;
            this.listaProdutosDataGridView.Size = new System.Drawing.Size(543, 299);
            this.listaProdutosDataGridView.TabIndex = 20;
            // 
            // Código
            // 
            this.Código.HeaderText = "Código";
            this.Código.Name = "Código";
            this.Código.ReadOnly = true;
            // 
            // Descrição
            // 
            this.Descrição.HeaderText = "Descrição";
            this.Descrição.Name = "Descrição";
            this.Descrição.ReadOnly = true;
            // 
            // Quantidade
            // 
            this.Quantidade.HeaderText = "Quantidade";
            this.Quantidade.Name = "Quantidade";
            this.Quantidade.ReadOnly = true;
            // 
            // valor
            // 
            this.valor.HeaderText = "Preço Unit.";
            this.valor.Name = "valor";
            this.valor.ReadOnly = true;
            // 
            // valorTotalProduto
            // 
            this.valorTotalProduto.HeaderText = "Valor Total";
            this.valorTotalProduto.Name = "valorTotalProduto";
            this.valorTotalProduto.ReadOnly = true;
            // 
            // quantidadeLabel
            // 
            this.quantidadeLabel.AutoSize = true;
            this.quantidadeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantidadeLabel.Location = new System.Drawing.Point(280, 88);
            this.quantidadeLabel.Name = "quantidadeLabel";
            this.quantidadeLabel.Size = new System.Drawing.Size(76, 13);
            this.quantidadeLabel.TabIndex = 19;
            this.quantidadeLabel.Text = "Quantidade:";
            // 
            // quantidadeTextBox
            // 
            this.quantidadeTextBox.Location = new System.Drawing.Point(280, 120);
            this.quantidadeTextBox.Name = "quantidadeTextBox";
            this.quantidadeTextBox.Size = new System.Drawing.Size(173, 20);
            this.quantidadeTextBox.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(251, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 22);
            this.label1.TabIndex = 17;
            this.label1.Text = "X";
            // 
            // codigoProdutoTextBox
            // 
            this.codigoProdutoTextBox.Location = new System.Drawing.Point(26, 121);
            this.codigoProdutoTextBox.Name = "codigoProdutoTextBox";
            this.codigoProdutoTextBox.Size = new System.Drawing.Size(209, 20);
            this.codigoProdutoTextBox.TabIndex = 16;
            // 
            // codigoProdutoLabel
            // 
            this.codigoProdutoLabel.AutoSize = true;
            this.codigoProdutoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codigoProdutoLabel.Location = new System.Drawing.Point(23, 88);
            this.codigoProdutoLabel.Name = "codigoProdutoLabel";
            this.codigoProdutoLabel.Size = new System.Drawing.Size(116, 13);
            this.codigoProdutoLabel.TabIndex = 14;
            this.codigoProdutoLabel.Text = "Código do Produto:";
            // 
            // valorTotalDinamicoLabel
            // 
            this.valorTotalDinamicoLabel.AutoSize = true;
            this.valorTotalDinamicoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.valorTotalDinamicoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valorTotalDinamicoLabel.Location = new System.Drawing.Point(714, 19);
            this.valorTotalDinamicoLabel.Name = "valorTotalDinamicoLabel";
            this.valorTotalDinamicoLabel.Padding = new System.Windows.Forms.Padding(30, 10, 30, 10);
            this.valorTotalDinamicoLabel.Size = new System.Drawing.Size(129, 53);
            this.valorTotalDinamicoLabel.TabIndex = 27;
            this.valorTotalDinamicoLabel.Text = "0.00";
            // 
            // valorTotalLabel
            // 
            this.valorTotalLabel.AutoSize = true;
            this.valorTotalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valorTotalLabel.Location = new System.Drawing.Point(594, 19);
            this.valorTotalLabel.Name = "valorTotalLabel";
            this.valorTotalLabel.Size = new System.Drawing.Size(73, 13);
            this.valorTotalLabel.TabIndex = 26;
            this.valorTotalLabel.Text = "Valor Total:";
            // 
            // operadorNameLabel
            // 
            this.operadorNameLabel.AutoSize = true;
            this.operadorNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.operadorNameLabel.Location = new System.Drawing.Point(299, 12);
            this.operadorNameLabel.Name = "operadorNameLabel";
            this.operadorNameLabel.Size = new System.Drawing.Size(73, 17);
            this.operadorNameLabel.TabIndex = 28;
            this.operadorNameLabel.Text = "Operador:";
            // 
            // voltarButton
            // 
            this.voltarButton.Location = new System.Drawing.Point(22, 12);
            this.voltarButton.Name = "voltarButton";
            this.voltarButton.Size = new System.Drawing.Size(100, 34);
            this.voltarButton.TabIndex = 31;
            this.voltarButton.Text = "Voltar";
            this.voltarButton.UseVisualStyleBackColor = true;
            this.voltarButton.Click += new System.EventHandler(this.voltarButton_Click);
            // 
            // produtoImagemPictureBox
            // 
            this.produtoImagemPictureBox.Location = new System.Drawing.Point(610, 88);
            this.produtoImagemPictureBox.Name = "produtoImagemPictureBox";
            this.produtoImagemPictureBox.Size = new System.Drawing.Size(258, 221);
            this.produtoImagemPictureBox.TabIndex = 15;
            this.produtoImagemPictureBox.TabStop = false;
            // 
            // fecharCaixaButton
            // 
            this.fecharCaixaButton.Location = new System.Drawing.Point(153, 12);
            this.fecharCaixaButton.Name = "fecharCaixaButton";
            this.fecharCaixaButton.Size = new System.Drawing.Size(101, 33);
            this.fecharCaixaButton.TabIndex = 32;
            this.fecharCaixaButton.Text = "Fechar Caixa";
            this.fecharCaixaButton.UseVisualStyleBackColor = true;
            this.fecharCaixaButton.Click += new System.EventHandler(this.fecharCaixaButton_Click);
            // 
            // Venda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(894, 512);
            this.Controls.Add(this.fecharCaixaButton);
            this.Controls.Add(this.voltarButton);
            this.Controls.Add(this.operadorNameLabel);
            this.Controls.Add(this.valorTotalDinamicoLabel);
            this.Controls.Add(this.valorTotalLabel);
            this.Controls.Add(this.addProdutoButton);
            this.Controls.Add(this.fecharCompraButton);
            this.Controls.Add(this.listaProdutosDataGridView);
            this.Controls.Add(this.quantidadeLabel);
            this.Controls.Add(this.quantidadeTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.codigoProdutoTextBox);
            this.Controls.Add(this.produtoImagemPictureBox);
            this.Controls.Add(this.codigoProdutoLabel);
            this.Name = "Venda";
            this.Text = "Caixa";
            this.Load += new System.EventHandler(this.Venda_Load);
            ((System.ComponentModel.ISupportInitialize)(this.listaProdutosDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.produtoImagemPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addProdutoButton;
        private System.Windows.Forms.Button fecharCompraButton;
        private System.Windows.Forms.DataGridView listaProdutosDataGridView;
        private System.Windows.Forms.Label quantidadeLabel;
        private System.Windows.Forms.TextBox quantidadeTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox codigoProdutoTextBox;
        private System.Windows.Forms.Label codigoProdutoLabel;
        private System.Windows.Forms.Label valorTotalDinamicoLabel;
        private System.Windows.Forms.Label valorTotalLabel;
        private System.Windows.Forms.Label operadorNameLabel;
        private System.Windows.Forms.Button voltarButton;
        private System.Windows.Forms.PictureBox produtoImagemPictureBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn Código;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descrição;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn valor;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorTotalProduto;
        private System.Windows.Forms.Button fecharCaixaButton;
    }
}