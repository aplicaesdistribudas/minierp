﻿namespace Mini_Erp
{
    partial class Usuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.registerUserButton = new System.Windows.Forms.Button();
            this.alterUserButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // registerUserButton
            // 
            this.registerUserButton.Location = new System.Drawing.Point(89, 26);
            this.registerUserButton.Name = "registerUserButton";
            this.registerUserButton.Size = new System.Drawing.Size(171, 52);
            this.registerUserButton.TabIndex = 0;
            this.registerUserButton.Text = "Cadastrar Usuário";
            this.registerUserButton.UseVisualStyleBackColor = true;
            this.registerUserButton.Click += new System.EventHandler(this.registerUserButton_Click);
            // 
            // alterUserButton
            // 
            this.alterUserButton.Location = new System.Drawing.Point(89, 111);
            this.alterUserButton.Name = "alterUserButton";
            this.alterUserButton.Size = new System.Drawing.Size(171, 52);
            this.alterUserButton.TabIndex = 1;
            this.alterUserButton.Text = "Alterar Usuário";
            this.alterUserButton.UseVisualStyleBackColor = true;
            this.alterUserButton.Click += new System.EventHandler(this.alterUserButton_Click);
            // 
            // Usuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 210);
            this.Controls.Add(this.alterUserButton);
            this.Controls.Add(this.registerUserButton);
            this.Name = "Usuario";
            this.Text = "Usuario";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Usuario_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button registerUserButton;
        private System.Windows.Forms.Button alterUserButton;
    }
}