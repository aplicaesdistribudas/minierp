﻿using Mini_Erp.DAO;
using Mini_Erp.Models;
using System;
using System.Windows.Forms;

namespace Mini_Erp
{
    public partial class CadastroAlteracaoUsuario : Form
    {
        private User user;

        public CadastroAlteracaoUsuario()
        {
            InitializeComponent();
            user = new User();
        }

        public CadastroAlteracaoUsuario(User user)
        {
            this.user = user;

            InitializeComponent();

            nomeTextBox.Text = user.name;
            nicknameTextBox.Text = user.nickname;
            emailTextBox.Text = user.email;
            cargoTextBox.Text = user.cargo;
            roleComboBox.SelectedIndex = (user.role - 1);
            if (Authentification.role != 1)
            {
                cargoTextBox.Hide();
                cargoLabel.Text = cargoLabel.Text + ": \n\n" + user.cargo;
                string[] roles = {"", "Admin", "Vendedor", "Estoquista", "Financeiro" };
                permissaoLabel.Text = permissaoLabel.Text + ": \n\n" + roles[user.role];
                roleComboBox.Hide();
            }
            passwordTextBox.Text = user.password;
            cadastrarButton.Text = "Alterar";

        }

        private void AlteracaoUsuario_Load(object sender, EventArgs e)
        {

        }

        private void cadastrarButton_Click(object sender, EventArgs e)
        {
            UserRepository repo = new UserRepository();
            user.name = nomeTextBox.Text;
            user.nickname = nicknameTextBox.Text;
            user.email = emailTextBox.Text;
            user.cargo = cargoTextBox.Text;
            user.password = passwordTextBox.Text;
            user.role = (roleComboBox.SelectedIndex + 1);
            if (cadastrarButton.Text.Equals("Alterar"))
            {
                repo.Update(user);
                MessageBox.Show("Dados do Usuário " + user.name + " atualizados com sucesso!");
            }
            else
            {
                repo.Add(user);
                MessageBox.Show("Dados do Usuário " + user.name + " cadastrados com sucesso!");
            }

            Index index = new Index();
            index.Show();
            Dispose();
        }

        private void voltarButton_Click(object sender, EventArgs e)
        {
            Index index = new Index();
            index.Show();
            Dispose();
        }

        private void CadastroAlteracaoUsuario_FormClosed(object sender, FormClosedEventArgs e)
        {
            Index index = new Index();
            index.Show();
        }
    }
}
