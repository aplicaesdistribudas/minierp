﻿namespace Mini_Erp
{
    partial class Index
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.caixaButton = new System.Windows.Forms.Button();
            this.produtosButton = new System.Windows.Forms.Button();
            this.usuariosButton = new System.Windows.Forms.Button();
            this.sairButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // caixaButton
            // 
            this.caixaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.caixaButton.Location = new System.Drawing.Point(12, 29);
            this.caixaButton.Name = "caixaButton";
            this.caixaButton.Size = new System.Drawing.Size(271, 152);
            this.caixaButton.TabIndex = 0;
            this.caixaButton.Text = "Caixa";
            this.caixaButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.caixaButton.UseVisualStyleBackColor = true;
            this.caixaButton.Click += new System.EventHandler(this.caixaButton_Click);
            // 
            // produtosButton
            // 
            this.produtosButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.produtosButton.Location = new System.Drawing.Point(293, 29);
            this.produtosButton.Name = "produtosButton";
            this.produtosButton.Size = new System.Drawing.Size(271, 152);
            this.produtosButton.TabIndex = 1;
            this.produtosButton.Text = "Produtos";
            this.produtosButton.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.produtosButton.UseVisualStyleBackColor = true;
            this.produtosButton.Click += new System.EventHandler(this.produtosButton_Click);
            // 
            // usuariosButton
            // 
            this.usuariosButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usuariosButton.Location = new System.Drawing.Point(12, 191);
            this.usuariosButton.Name = "usuariosButton";
            this.usuariosButton.Size = new System.Drawing.Size(271, 152);
            this.usuariosButton.TabIndex = 2;
            this.usuariosButton.Text = "Usuários";
            this.usuariosButton.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.usuariosButton.UseVisualStyleBackColor = true;
            this.usuariosButton.Click += new System.EventHandler(this.usuariosButton_Click);
            // 
            // sairButton
            // 
            this.sairButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sairButton.Location = new System.Drawing.Point(293, 191);
            this.sairButton.Name = "sairButton";
            this.sairButton.Size = new System.Drawing.Size(271, 152);
            this.sairButton.TabIndex = 3;
            this.sairButton.Text = "Sair";
            this.sairButton.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.sairButton.UseVisualStyleBackColor = true;
            this.sairButton.Click += new System.EventHandler(this.sairButton_Click);
            // 
            // Index
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 359);
            this.Controls.Add(this.sairButton);
            this.Controls.Add(this.usuariosButton);
            this.Controls.Add(this.produtosButton);
            this.Controls.Add(this.caixaButton);
            this.Name = "Index";
            this.Text = "Tela Principal";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Index_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button caixaButton;
        private System.Windows.Forms.Button produtosButton;
        private System.Windows.Forms.Button usuariosButton;
        private System.Windows.Forms.Button sairButton;
    }
}