﻿namespace Mini_Erp
{
    partial class AbrirCaixa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.valorAberturaTextBox = new System.Windows.Forms.TextBox();
            this.abrirCaixaButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(198, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Digite um valor para a abertura do caixa:";
            // 
            // valorAberturaTextBox
            // 
            this.valorAberturaTextBox.Location = new System.Drawing.Point(16, 49);
            this.valorAberturaTextBox.Name = "valorAberturaTextBox";
            this.valorAberturaTextBox.Size = new System.Drawing.Size(305, 20);
            this.valorAberturaTextBox.TabIndex = 1;
            // 
            // abrirCaixaButton
            // 
            this.abrirCaixaButton.Location = new System.Drawing.Point(239, 84);
            this.abrirCaixaButton.Name = "abrirCaixaButton";
            this.abrirCaixaButton.Size = new System.Drawing.Size(75, 23);
            this.abrirCaixaButton.TabIndex = 2;
            this.abrirCaixaButton.Text = "Abrir";
            this.abrirCaixaButton.UseVisualStyleBackColor = true;
            this.abrirCaixaButton.Click += new System.EventHandler(this.abrirCaixaButton_Click);
            // 
            // AbrirCaixa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(333, 119);
            this.Controls.Add(this.abrirCaixaButton);
            this.Controls.Add(this.valorAberturaTextBox);
            this.Controls.Add(this.label1);
            this.Name = "AbrirCaixa";
            this.Text = "Abrir Caixa";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox valorAberturaTextBox;
        private System.Windows.Forms.Button abrirCaixaButton;
    }
}