﻿namespace Mini_Erp
{
    partial class SelecaoUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userNicknameLabel = new System.Windows.Forms.Label();
            this.userNicknameTextBox = new System.Windows.Forms.TextBox();
            this.editUserButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // userNicknameLabel
            // 
            this.userNicknameLabel.AutoSize = true;
            this.userNicknameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userNicknameLabel.Location = new System.Drawing.Point(96, 19);
            this.userNicknameLabel.Name = "userNicknameLabel";
            this.userNicknameLabel.Size = new System.Drawing.Size(163, 13);
            this.userNicknameLabel.TabIndex = 0;
            this.userNicknameLabel.Text = "Digite o apelido do usuário:";
            // 
            // userNicknameTextBox
            // 
            this.userNicknameTextBox.Location = new System.Drawing.Point(126, 66);
            this.userNicknameTextBox.Name = "userNicknameTextBox";
            this.userNicknameTextBox.Size = new System.Drawing.Size(100, 20);
            this.userNicknameTextBox.TabIndex = 1;
            // 
            // editUserButton
            // 
            this.editUserButton.Location = new System.Drawing.Point(126, 116);
            this.editUserButton.Name = "editUserButton";
            this.editUserButton.Size = new System.Drawing.Size(100, 48);
            this.editUserButton.TabIndex = 2;
            this.editUserButton.Text = "Editar";
            this.editUserButton.UseVisualStyleBackColor = true;
            this.editUserButton.Click += new System.EventHandler(this.editUserButton_Click);
            // 
            // SelecaoUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 210);
            this.Controls.Add(this.editUserButton);
            this.Controls.Add(this.userNicknameTextBox);
            this.Controls.Add(this.userNicknameLabel);
            this.Name = "SelecaoUsuario";
            this.Text = "SelecaoUsuario";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label userNicknameLabel;
        private System.Windows.Forms.TextBox userNicknameTextBox;
        private System.Windows.Forms.Button editUserButton;
    }
}