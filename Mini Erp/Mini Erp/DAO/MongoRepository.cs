﻿using Mini_Erp.Classes;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mini_Erp.DAO
{
#pragma warning disable CS0649
    class MongoRepository<T> : IMongoDAO<T>
    where T : class, IEntity
    {
        private static IMongoDatabase _db;
        protected static string TypeCollection = string.Empty;

        public MongoRepository()
        {
            _db = MongoConnection.getDatabase();
        }
        public void Add(T entity)
        {
            GetCollection.InsertOne(entity);
        }

        public  T findOne(FilterDefinition<T> filter)
        {
            try {
               return GetCollection.Find(filter).First<T>();
            }
            catch (Exception)
            {
                return null;
            }
        }


        public IEnumerable<T> All()
        {
            return GetCollection.Find<T>(new BsonDocument()).ToList<T>();
        }

        public IEnumerable<T> AllAs(ProjectionDefinition<T> fields)
        {
            return GetCollection.Find<T>(new BsonDocument()).Project<T>(fields).ToList<T>(); ;
        }
        

        public void Delete(T entity)
        {
            var models = new WriteModel<T>[]
            {
                new DeleteOneModel<T>(new BsonDocument("_id", entity.getId()))
            };
            GetCollection.BulkWrite(models);
        }

        public void Update(T entity)
        {
            //GetCollection.ReplaceOne(doc => doc.getId() == entity.getId(), entity);

            var repo = new MongoRepository<T>();
            var filterBuilder = Builders<T>.Filter;
            var filter = filterBuilder.Eq("_id", entity.getId());
            repo.Delete(entity);
            repo.Add(entity);

        }

        IMongoCollection<T> GetCollection
        {
            get { return _db.GetCollection<T>(TypeCollection); }
        }

        
    }
}
