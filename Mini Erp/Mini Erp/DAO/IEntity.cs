﻿
using MongoDB.Bson;

namespace Mini_Erp.DAO
{
    internal interface IEntity
    {
        string getCollection();
        ObjectId getId();
    }
}