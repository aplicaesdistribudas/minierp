﻿using Mini_Erp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini_Erp.DAO
{
    class CashierRepository : MongoRepository<Cashier>
    {

        public CashierRepository()
        : base()
        {
            TypeCollection = "cashiers";
        }
        

    }
}
