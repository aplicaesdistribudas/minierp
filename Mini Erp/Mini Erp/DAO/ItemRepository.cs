﻿using Mini_Erp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini_Erp.DAO
{
    class ItemRepository : MongoRepository<Item>
    {
        public ItemRepository()
        : base()
        {
            TypeCollection = "itens";
        }
    }
}
