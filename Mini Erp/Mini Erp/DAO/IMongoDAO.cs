﻿using Mini_Erp.Models;
using System.Collections;
using System.Collections.Generic;

namespace Mini_Erp.DAO
{
    public interface IMongoDAO<T> where T : class
    {
        
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);
        IEnumerable<T> All();

    }
}