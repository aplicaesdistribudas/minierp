﻿using Mini_Erp.Models;

namespace Mini_Erp.DAO
{
    class ProductRepository : MongoRepository<Product>
    {

        public ProductRepository()
        : base()
        {
            TypeCollection = "products";
        }


    }
}
