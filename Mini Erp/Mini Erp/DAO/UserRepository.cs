﻿using Mini_Erp.Models;

namespace Mini_Erp.DAO
{
    class UserRepository : MongoRepository<User>
    { 

        public UserRepository()
        : base()
        {
            TypeCollection = "users";
        }

        
    }
}
