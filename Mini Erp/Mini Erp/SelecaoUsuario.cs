﻿using Mini_Erp.Classes;
using Mini_Erp.DAO;
using Mini_Erp.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mini_Erp
{
    public partial class SelecaoUsuario : Form
    {
        public SelecaoUsuario()
        {
            InitializeComponent();
        }

        private void editUserButton_Click(object sender, EventArgs e)
        {
            var users = new UserRepository();
            var filterBuilder = Builders<User>.Filter;
            var filter = filterBuilder.Eq("nickname", userNicknameTextBox.Text);
            try
            {
                var user = users.findOne(filter);

                CadastroAlteracaoUsuario alteracaoUsuario = new CadastroAlteracaoUsuario(user);
                alteracaoUsuario.Show();
                this.Dispose();
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("Usuário não encontrado");
                return;
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Usuário não encontrado");
                return;
            }
        }
    }
}
