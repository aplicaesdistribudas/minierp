﻿namespace Mini_Erp
{
    partial class FecharCaixa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usuarioLabel = new System.Windows.Forms.Label();
            this.userTextBox = new System.Windows.Forms.TextBox();
            this.aberturaValorTextBox = new System.Windows.Forms.TextBox();
            this.valorAberturaLabel = new System.Windows.Forms.Label();
            this.horarioAberturaTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.valorFechamentoTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.horarioFechamentoTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.quantidadeVendasTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.quantidadeProdutosTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.sairLabel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // usuarioLabel
            // 
            this.usuarioLabel.AutoSize = true;
            this.usuarioLabel.Location = new System.Drawing.Point(136, 125);
            this.usuarioLabel.Name = "usuarioLabel";
            this.usuarioLabel.Size = new System.Drawing.Size(43, 13);
            this.usuarioLabel.TabIndex = 0;
            this.usuarioLabel.Text = "Usuário";
            // 
            // userTextBox
            // 
            this.userTextBox.Location = new System.Drawing.Point(198, 122);
            this.userTextBox.Name = "userTextBox";
            this.userTextBox.Size = new System.Drawing.Size(114, 20);
            this.userTextBox.TabIndex = 1;
            // 
            // aberturaValorTextBox
            // 
            this.aberturaValorTextBox.Location = new System.Drawing.Point(198, 164);
            this.aberturaValorTextBox.Name = "aberturaValorTextBox";
            this.aberturaValorTextBox.Size = new System.Drawing.Size(114, 20);
            this.aberturaValorTextBox.TabIndex = 3;
            // 
            // valorAberturaLabel
            // 
            this.valorAberturaLabel.AutoSize = true;
            this.valorAberturaLabel.Location = new System.Drawing.Point(90, 164);
            this.valorAberturaLabel.Name = "valorAberturaLabel";
            this.valorAberturaLabel.Size = new System.Drawing.Size(89, 13);
            this.valorAberturaLabel.TabIndex = 2;
            this.valorAberturaLabel.Text = "Valor de Abertura";
            // 
            // horarioAberturaTextBox
            // 
            this.horarioAberturaTextBox.Location = new System.Drawing.Point(198, 206);
            this.horarioAberturaTextBox.Name = "horarioAberturaTextBox";
            this.horarioAberturaTextBox.Size = new System.Drawing.Size(114, 20);
            this.horarioAberturaTextBox.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(80, 206);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Horário de Abertura";
            // 
            // valorFechamentoTextBox
            // 
            this.valorFechamentoTextBox.Location = new System.Drawing.Point(198, 248);
            this.valorFechamentoTextBox.Name = "valorFechamentoTextBox";
            this.valorFechamentoTextBox.Size = new System.Drawing.Size(114, 20);
            this.valorFechamentoTextBox.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 251);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Valor de Fechamento";
            // 
            // horarioFechamentoTextBox
            // 
            this.horarioFechamentoTextBox.Location = new System.Drawing.Point(198, 293);
            this.horarioFechamentoTextBox.Name = "horarioFechamentoTextBox";
            this.horarioFechamentoTextBox.Size = new System.Drawing.Size(114, 20);
            this.horarioFechamentoTextBox.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 296);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Horário de Fechamento";
            // 
            // quantidadeVendasTextBox
            // 
            this.quantidadeVendasTextBox.Location = new System.Drawing.Point(198, 340);
            this.quantidadeVendasTextBox.Name = "quantidadeVendasTextBox";
            this.quantidadeVendasTextBox.Size = new System.Drawing.Size(114, 20);
            this.quantidadeVendasTextBox.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(62, 343);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Quantidade de Vendas";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(108, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(204, 31);
            this.label5.TabIndex = 12;
            this.label5.Text = "Caixa Fechado.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(185, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Informações de fechamento do caixa:";
            // 
            // quantidadeProdutosTextBox
            // 
            this.quantidadeProdutosTextBox.Location = new System.Drawing.Point(198, 388);
            this.quantidadeProdutosTextBox.Name = "quantidadeProdutosTextBox";
            this.quantidadeProdutosTextBox.Size = new System.Drawing.Size(114, 20);
            this.quantidadeProdutosTextBox.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 388);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(167, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Quantidade de produtos vendidos";
            // 
            // sairLabel
            // 
            this.sairLabel.Location = new System.Drawing.Point(260, 487);
            this.sairLabel.Name = "sairLabel";
            this.sairLabel.Size = new System.Drawing.Size(108, 36);
            this.sairLabel.TabIndex = 16;
            this.sairLabel.Text = "Sair";
            this.sairLabel.UseVisualStyleBackColor = true;
            this.sairLabel.Click += new System.EventHandler(this.sairLabel_Click);
            // 
            // FecharCaixa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(404, 560);
            this.Controls.Add(this.sairLabel);
            this.Controls.Add(this.quantidadeProdutosTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.quantidadeVendasTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.horarioFechamentoTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.valorFechamentoTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.horarioAberturaTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.aberturaValorTextBox);
            this.Controls.Add(this.valorAberturaLabel);
            this.Controls.Add(this.userTextBox);
            this.Controls.Add(this.usuarioLabel);
            this.Name = "FecharCaixa";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label usuarioLabel;
        private System.Windows.Forms.TextBox userTextBox;
        private System.Windows.Forms.TextBox aberturaValorTextBox;
        private System.Windows.Forms.Label valorAberturaLabel;
        private System.Windows.Forms.TextBox horarioAberturaTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox valorFechamentoTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox horarioFechamentoTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox quantidadeVendasTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox quantidadeProdutosTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button sairLabel;
    }
}