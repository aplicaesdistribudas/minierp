﻿namespace Mini_Erp
{
    partial class CadastroProdutos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastroProdutos));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.inserirTab = new System.Windows.Forms.TabPage();
            this.voltarButton = new System.Windows.Forms.Button();
            this.precoTextBox = new System.Windows.Forms.TextBox();
            this.validadeMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.insertProductButton = new System.Windows.Forms.Button();
            this.precoLabel = new System.Windows.Forms.Label();
            this.adicionarImagemButton = new System.Windows.Forms.Button();
            this.adicionarImagemPictureBox = new System.Windows.Forms.PictureBox();
            this.quantidadeUpDown = new System.Windows.Forms.NumericUpDown();
            this.quantidadeLabel = new System.Windows.Forms.Label();
            this.validadeLabel = new System.Windows.Forms.Label();
            this.descricaoTextBox = new System.Windows.Forms.TextBox();
            this.descricaoLabel = new System.Windows.Forms.Label();
            this.codigoTextBox = new System.Windows.Forms.TextBox();
            this.codigoLabel = new System.Windows.Forms.Label();
            this.produtosTab = new System.Windows.Forms.TabPage();
            this.updateProductButton = new System.Windows.Forms.Button();
            this.productDataGridView = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.inserirTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.adicionarImagemPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantidadeUpDown)).BeginInit();
            this.produtosTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.inserirTab);
            this.tabControl1.Controls.Add(this.produtosTab);
            this.tabControl1.Location = new System.Drawing.Point(-1, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(645, 477);
            this.tabControl1.TabIndex = 0;
            // 
            // inserirTab
            // 
            this.inserirTab.Controls.Add(this.voltarButton);
            this.inserirTab.Controls.Add(this.precoTextBox);
            this.inserirTab.Controls.Add(this.validadeMaskedTextBox);
            this.inserirTab.Controls.Add(this.insertProductButton);
            this.inserirTab.Controls.Add(this.precoLabel);
            this.inserirTab.Controls.Add(this.adicionarImagemButton);
            this.inserirTab.Controls.Add(this.adicionarImagemPictureBox);
            this.inserirTab.Controls.Add(this.quantidadeUpDown);
            this.inserirTab.Controls.Add(this.quantidadeLabel);
            this.inserirTab.Controls.Add(this.validadeLabel);
            this.inserirTab.Controls.Add(this.descricaoTextBox);
            this.inserirTab.Controls.Add(this.descricaoLabel);
            this.inserirTab.Controls.Add(this.codigoTextBox);
            this.inserirTab.Controls.Add(this.codigoLabel);
            this.inserirTab.Location = new System.Drawing.Point(4, 22);
            this.inserirTab.Name = "inserirTab";
            this.inserirTab.Padding = new System.Windows.Forms.Padding(3);
            this.inserirTab.Size = new System.Drawing.Size(637, 451);
            this.inserirTab.TabIndex = 0;
            this.inserirTab.Text = "Inserir";
            this.inserirTab.UseVisualStyleBackColor = true;
            // 
            // voltarButton
            // 
            this.voltarButton.Location = new System.Drawing.Point(398, 395);
            this.voltarButton.Name = "voltarButton";
            this.voltarButton.Size = new System.Drawing.Size(100, 34);
            this.voltarButton.TabIndex = 30;
            this.voltarButton.Text = "Voltar";
            this.voltarButton.UseVisualStyleBackColor = true;
            this.voltarButton.Click += new System.EventHandler(this.voltarButton_Click);
            // 
            // precoTextBox
            // 
            this.precoTextBox.Location = new System.Drawing.Point(51, 234);
            this.precoTextBox.Name = "precoTextBox";
            this.precoTextBox.Size = new System.Drawing.Size(136, 20);
            this.precoTextBox.TabIndex = 29;
            // 
            // validadeMaskedTextBox
            // 
            this.validadeMaskedTextBox.Location = new System.Drawing.Point(51, 315);
            this.validadeMaskedTextBox.Mask = "00/00/0000";
            this.validadeMaskedTextBox.Name = "validadeMaskedTextBox";
            this.validadeMaskedTextBox.Size = new System.Drawing.Size(136, 20);
            this.validadeMaskedTextBox.TabIndex = 28;
            this.validadeMaskedTextBox.ValidatingType = typeof(System.DateTime);
            // 
            // insertProductButton
            // 
            this.insertProductButton.Location = new System.Drawing.Point(512, 395);
            this.insertProductButton.Name = "insertProductButton";
            this.insertProductButton.Size = new System.Drawing.Size(102, 34);
            this.insertProductButton.TabIndex = 27;
            this.insertProductButton.Text = "Inserir Produto";
            this.insertProductButton.UseVisualStyleBackColor = true;
            this.insertProductButton.Click += new System.EventHandler(this.insertProductButton_Click_1);
            // 
            // precoLabel
            // 
            this.precoLabel.AutoSize = true;
            this.precoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.precoLabel.Location = new System.Drawing.Point(47, 205);
            this.precoLabel.Name = "precoLabel";
            this.precoLabel.Size = new System.Drawing.Size(40, 13);
            this.precoLabel.TabIndex = 26;
            this.precoLabel.Text = "Preço";
            // 
            // adicionarImagemButton
            // 
            this.adicionarImagemButton.Location = new System.Drawing.Point(553, 231);
            this.adicionarImagemButton.Name = "adicionarImagemButton";
            this.adicionarImagemButton.Size = new System.Drawing.Size(75, 23);
            this.adicionarImagemButton.TabIndex = 25;
            this.adicionarImagemButton.Text = "Escolher";
            this.adicionarImagemButton.UseVisualStyleBackColor = true;
            this.adicionarImagemButton.Click += new System.EventHandler(this.adicionarImagemButton_Click_1);
            // 
            // adicionarImagemPictureBox
            // 
            this.adicionarImagemPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("adicionarImagemPictureBox.Image")));
            this.adicionarImagemPictureBox.InitialImage = null;
            this.adicionarImagemPictureBox.Location = new System.Drawing.Point(387, 22);
            this.adicionarImagemPictureBox.Name = "adicionarImagemPictureBox";
            this.adicionarImagemPictureBox.Size = new System.Drawing.Size(241, 203);
            this.adicionarImagemPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.adicionarImagemPictureBox.TabIndex = 24;
            this.adicionarImagemPictureBox.TabStop = false;
            // 
            // quantidadeUpDown
            // 
            this.quantidadeUpDown.Location = new System.Drawing.Point(268, 234);
            this.quantidadeUpDown.Name = "quantidadeUpDown";
            this.quantidadeUpDown.Size = new System.Drawing.Size(64, 20);
            this.quantidadeUpDown.TabIndex = 23;
            // 
            // quantidadeLabel
            // 
            this.quantidadeLabel.AutoSize = true;
            this.quantidadeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantidadeLabel.Location = new System.Drawing.Point(265, 205);
            this.quantidadeLabel.Name = "quantidadeLabel";
            this.quantidadeLabel.Size = new System.Drawing.Size(72, 13);
            this.quantidadeLabel.TabIndex = 22;
            this.quantidadeLabel.Text = "Quantidade";
            // 
            // validadeLabel
            // 
            this.validadeLabel.AutoSize = true;
            this.validadeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.validadeLabel.Location = new System.Drawing.Point(47, 277);
            this.validadeLabel.Name = "validadeLabel";
            this.validadeLabel.Size = new System.Drawing.Size(56, 13);
            this.validadeLabel.TabIndex = 21;
            this.validadeLabel.Text = "Validade";
            // 
            // descricaoTextBox
            // 
            this.descricaoTextBox.Location = new System.Drawing.Point(50, 145);
            this.descricaoTextBox.Name = "descricaoTextBox";
            this.descricaoTextBox.Size = new System.Drawing.Size(256, 20);
            this.descricaoTextBox.TabIndex = 20;
            // 
            // descricaoLabel
            // 
            this.descricaoLabel.AutoSize = true;
            this.descricaoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descricaoLabel.Location = new System.Drawing.Point(47, 107);
            this.descricaoLabel.Name = "descricaoLabel";
            this.descricaoLabel.Size = new System.Drawing.Size(64, 13);
            this.descricaoLabel.TabIndex = 19;
            this.descricaoLabel.Text = "Descrição";
            // 
            // codigoTextBox
            // 
            this.codigoTextBox.Location = new System.Drawing.Point(50, 55);
            this.codigoTextBox.Name = "codigoTextBox";
            this.codigoTextBox.Size = new System.Drawing.Size(204, 20);
            this.codigoTextBox.TabIndex = 18;
            // 
            // codigoLabel
            // 
            this.codigoLabel.AutoSize = true;
            this.codigoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codigoLabel.Location = new System.Drawing.Point(48, 22);
            this.codigoLabel.Name = "codigoLabel";
            this.codigoLabel.Size = new System.Drawing.Size(46, 13);
            this.codigoLabel.TabIndex = 17;
            this.codigoLabel.Text = "Código";
            // 
            // produtosTab
            // 
            this.produtosTab.Controls.Add(this.updateProductButton);
            this.produtosTab.Controls.Add(this.productDataGridView);
            this.produtosTab.Location = new System.Drawing.Point(4, 22);
            this.produtosTab.Name = "produtosTab";
            this.produtosTab.Padding = new System.Windows.Forms.Padding(3);
            this.produtosTab.Size = new System.Drawing.Size(637, 451);
            this.produtosTab.TabIndex = 1;
            this.produtosTab.Text = "Produtos";
            this.produtosTab.UseVisualStyleBackColor = true;
            // 
            // updateProductButton
            // 
            this.updateProductButton.Location = new System.Drawing.Point(253, 413);
            this.updateProductButton.Name = "updateProductButton";
            this.updateProductButton.Size = new System.Drawing.Size(116, 23);
            this.updateProductButton.TabIndex = 1;
            this.updateProductButton.Text = "Atualizar Produtos";
            this.updateProductButton.UseVisualStyleBackColor = true;
            this.updateProductButton.Click += new System.EventHandler(this.updateProductButton_Click);
            // 
            // productDataGridView
            // 
            this.productDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.productDataGridView.Location = new System.Drawing.Point(0, 0);
            this.productDataGridView.Name = "productDataGridView";
            this.productDataGridView.Size = new System.Drawing.Size(637, 407);
            this.productDataGridView.TabIndex = 0;
            // 
            // CadastroProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 477);
            this.Controls.Add(this.tabControl1);
            this.Name = "CadastroProdutos";
            this.Text = "Cadastro de Produtos";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CadastroProdutos_FormClosed);
            this.Load += new System.EventHandler(this.CadastroProdutos_Load);
            this.tabControl1.ResumeLayout(false);
            this.inserirTab.ResumeLayout(false);
            this.inserirTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.adicionarImagemPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantidadeUpDown)).EndInit();
            this.produtosTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.productDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage inserirTab;
        private System.Windows.Forms.TextBox precoTextBox;
        private System.Windows.Forms.MaskedTextBox validadeMaskedTextBox;
        private System.Windows.Forms.Button insertProductButton;
        private System.Windows.Forms.Label precoLabel;
        private System.Windows.Forms.Button adicionarImagemButton;
        private System.Windows.Forms.PictureBox adicionarImagemPictureBox;
        private System.Windows.Forms.NumericUpDown quantidadeUpDown;
        private System.Windows.Forms.Label quantidadeLabel;
        private System.Windows.Forms.Label validadeLabel;
        private System.Windows.Forms.TextBox descricaoTextBox;
        private System.Windows.Forms.Label descricaoLabel;
        private System.Windows.Forms.TextBox codigoTextBox;
        private System.Windows.Forms.Label codigoLabel;
        private System.Windows.Forms.TabPage produtosTab;
        private System.Windows.Forms.DataGridView productDataGridView;
        private System.Windows.Forms.Button voltarButton;
        private System.Windows.Forms.Button updateProductButton;
    }
}