﻿using Mini_Erp.Classes;
using Mini_Erp.Models;
using MongoDB.Driver;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Mini_Erp
{
    public partial class Index : Form
    {
        public Index()
        {
            InitializeComponent();
        }

        private void caixaButton_Click(object sender, EventArgs e)
        {
            if ("3".Equals(Authentification.role))
            {
                MessageBox.Show("Você não possui permissão!!", "Permissão Negada");
                return;

            }

            if (StaticCashier.isReadyOpen())
            {
                Venda caixa = new Venda();
                caixa.Show();
            }
        }

        private void produtosButton_Click(object sender, EventArgs e)
        {
            if ("2".Equals(Authentification.role))
            {
                MessageBox.Show("Você não possui permissão!!", "Permissão Negada");
                return;

            }
            CadastroProdutos cadastro = new CadastroProdutos();
            cadastro.Show();
            Dispose();
        }

        private void usuariosButton_Click(object sender, EventArgs e)
        {
            if (Authentification.role != 1)
            {
                var users = MongoConnection.getDatabase().GetCollection<User>("users");
                var filterBuilder = Builders<User>.Filter;
                var filter = filterBuilder.Eq("_id", Authentification.id);
                var user = users.Find(filter).First();

                CadastroAlteracaoUsuario alteracaoUsuario = new CadastroAlteracaoUsuario(user);
                alteracaoUsuario.Show();
                Dispose();
            }
            else {
                Usuario usuario = new Usuario();
                usuario.Show();
                Dispose();
            }
        }

        private void sairButton_Click(object sender, EventArgs e)
        {
            Authentification.logout();
            Login login = new Login();
            login.Show();
            Dispose();
        }


        private void Index_FormClosed(object sender, FormClosedEventArgs e)
        {
            Authentification.logout();
            Login login = new Login();
            login.Show();
            Dispose();
            
        }
    }
}
