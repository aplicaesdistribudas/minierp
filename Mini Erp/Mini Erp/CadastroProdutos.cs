﻿using Mini_Erp.DAO;
using Mini_Erp.Models;
using MongoDB.Driver;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Mini_Erp
{
    public partial class CadastroProdutos : Form
    {
        private static string pathToFile;

        public CadastroProdutos()
        {
            InitializeComponent();

            if (Authentification.role != 1)
            {
                updateProductButton.Dispose();
                inserirTab.Dispose();
            }
        }

        private void CadastroProdutos_Load(object sender, EventArgs e)
        {
            ProductRepository repo = new ProductRepository();
            Product user = new Product();
            var products = repo.All();
            var source = new BindingSource();
            source.DataSource = products;
            productDataGridView.AutoGenerateColumns = true;
            productDataGridView.DataSource = source;
            productDataGridView.Columns[0].Visible = false;
        }

        private void adicionarImagemButton_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog fDialog = new OpenFileDialog();
            fDialog.Title = "Selecione a imagem do produto";
            fDialog.Filter = "Image Files (*.bmp, *.jpg, *.png)|*.bmp;*.jpg; *.png";
            fDialog.InitialDirectory = Environment.GetEnvironmentVariable("USERPROFILE") + "\\Pictures";

            if (fDialog.ShowDialog() == DialogResult.OK)
            {
                //Application.StartupPath + "\\images\\" + 
                pathToFile = Application.StartupPath + "\\images\\" + fDialog.SafeFileName;
                var sourceFile = fDialog.FileName;
                Image image = Image.FromFile(sourceFile);
                adicionarImagemPictureBox.Image = image;
                adicionarImagemPictureBox.Width = 241;
                adicionarImagemPictureBox.Height = 203;
                fDialog.Dispose();
                System.IO.File.Copy(sourceFile, pathToFile, true);
            }

            this.CadastroProdutos_Load(sender, e);
        }

        private void insertProductButton_Click_1(object sender, EventArgs e)
        {
            Product product = new Product();
            ProductRepository repoProduct = new ProductRepository();
            product.picture = pathToFile;
            product.code = codigoTextBox.Text;
            product.description = descricaoTextBox.Text;
            product.validate = Convert.ToDateTime(validadeMaskedTextBox.Text);
            product.quantity = int.Parse(quantidadeUpDown.Value.ToString());

            double price = 0.00;

            if (!double.TryParse(precoTextBox.Text, out price))
            {
                MessageBox.Show("Valor inválido! Por favor coloque o valor no formato: 0.00");
            }
            product.price = double.Parse(precoTextBox.Text);

            var filterBuilder = Builders<Product>.Filter;
            var filter = filterBuilder.Eq("code", codigoTextBox.Text);
            if (repoProduct.findOne(filter) != null)
            {
                MessageBox.Show("Código já cadastrado!");
                return;
            }

            repoProduct.Add(product);

            string message = "Deseja inserir um novo produto?";
            string caption = "Produto cadastrado com sucesso!";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            // Displays the MessageBox.

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.No)
            {
                Index index = new Index();
                index.Show();
                Dispose();
            }

            pathToFile = "";
            codigoTextBox.Text = "";
            descricaoTextBox.Text = "";
            validadeMaskedTextBox.Text = "";
            quantidadeUpDown.Value = 0;
            precoTextBox.Text = "";

        }

        private void voltarButton_Click(object sender, EventArgs e)
        {
            Index index = new Index();
            index.Show();
            Dispose();
        }

        private void CadastroProdutos_FormClosed(object sender, FormClosedEventArgs e)
        {
            Index index = new Index();
            index.Show();
            Dispose();
        }

        private void updateProductButton_Click(object sender, EventArgs e)
        {
            ProductRepository repo = new ProductRepository();
            Product product = new Product();
            var filterBuilder = Builders<Product>.Filter;
            var filter = filterBuilder.Eq("code", "");

            string message = "Deseja atualizar os produtos?";
            string caption = "Produto cadastrado com sucesso!";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            // Displays the MessageBox.

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.No)
            {
                return;
            }

            for (var i = 0; i < (productDataGridView.Rows.Count - 1); i++)
            {
                filter = filterBuilder.Eq("code", productDataGridView.Rows[i].Cells[2].Value);
                product = repo.findOne(filter);
                product.description = productDataGridView.Rows[i].Cells[1].Value.ToString();
                product.quantity = Convert.ToInt32(productDataGridView.Rows[i].Cells[4].Value);
                product.price = Convert.ToDouble(productDataGridView.Rows[i].Cells[5].Value);
                repo.Update(product);
            }
            MessageBox.Show("Produtos atualizados com sucesso!");
        }
    }
}
