﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini_Erp.Classes
{
    static class StaticCashier
    {
        public static ObjectId userId = Authentification.id;
        public static double openingValue { get; set; }
        public static double shutdownValue { get; set; }
        public static DateTime openingDateTime { get; set; }
        public static DateTime shutdownDateTime { get; set; }
        public static int productsQuatitySelledInDay { get; set; }
        public static double valueSelledInDay { get; set; }
        public static bool isColosed { get; set; }
        public static int venda { get; set; }

        public static bool isReadyOpen()
        {
            if (openingDateTime == null || openingValue == 0)
            {
                AbrirCaixa abertura = new AbrirCaixa();
                abertura.Show();
                return false;
            }
            return true;
        }


        public static void open(double openingValueIn)
        {
            openingValue = openingValueIn;
            openingDateTime = DateTime.Now;
            isColosed = false;
        }

        public static void close()
        {
            shutdownDateTime = DateTime.Now;
            shutdownValue += openingValue;
            isColosed = true;
        }
    }
}
