﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini_Erp
{
    class Users
    {

        public Users(string name, string email, string role, string password)
        {
            this.name = name;

            this.email = email;

            this.role = role;

            this.password = password;
        }

        public string name { get; set; }

        public string email { get; set; }

        public string password { get; set; }

        public string role { get; set; }

    }
}
