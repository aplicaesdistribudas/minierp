﻿using MongoDB.Bson;
using MongoDB.Driver;

namespace Mini_Erp.Classes
{
    public class MongoConnection
    {

        private static MongoClient getConnection()
        {
            return new MongoClient("mongodb://localhost:27017");
           
        }

        public static IMongoDatabase getDatabase()
        {
            var connection = getConnection();
            return  connection.GetDatabase("miniErp");
        }

        public static IMongoCollection<IConvertibleToBsonDocument> getCollection(string collection)
        {
            return getDatabase().GetCollection<IConvertibleToBsonDocument>(collection);
        }

    }
}
