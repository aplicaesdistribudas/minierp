using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mini_Erp
{
    static class Authentification
    {
        public static ObjectId id;
        public static string name;
        public static string password;
        public static int role;

        public static void login(ObjectId userId, string userName, string userPassword, int userRole)
        {
            id = userId;
            name = userName;
            password = userPassword;
            role = userRole;
        } 

        public  static void logout()
        {
            name = null;
            password = null;
            role = 0;
        }

    }
}
