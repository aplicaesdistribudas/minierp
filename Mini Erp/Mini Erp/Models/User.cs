﻿using Mini_Erp.DAO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections;

namespace Mini_Erp.Models
{
    public class User : IConvertibleToBsonDocument,IEntity
    {
        [BsonElement("_id")]
        public ObjectId id { get; set; }
        [BsonElement("name")]
        public string name { get; set; }
        [BsonElement("nickname")]
        public string nickname { get; set; }
        [BsonElement("email")]
        public string email { get; set; }
        [BsonElement("cargo")]
        public string cargo { get; set; }
        [BsonElement("role")]
        public int role { get; set; }
        [BsonElement("password")]
        public string password { get; set; }

        public string getCollection()
        {
            return "users";
        }

        public ObjectId getId()
        {
            return id;
        }

        public BsonDocument ToBsonDocument()
        {
            return new BsonDocument {
                { "_id", id },
                { "name", name },
                { "email", email },
                { "role", role },
                { "password", password }
                };
        }

        
    }
}
