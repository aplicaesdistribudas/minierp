﻿using Mini_Erp.DAO;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini_Erp.Models
{
    class Sell : IConvertibleToBsonDocument, IEntity
    {
        public ObjectId id;
        public ObjectId seller;
        public List<Product> products = new List<Product>();
        public DateTime created_at;


        public BsonDocument ToBsonDocument()
        {
            return new BsonDocument {
                { "_id", id },
                { "seller", seller },
                { "created_at", created_at }
                };
        }

        public string getCollection()
        {
            return "selles";
        }

        public ObjectId getId()
        {
            return id;
        }
    }
}
