﻿using Mini_Erp.DAO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections;

namespace Mini_Erp.Models
{
    public class Item : IConvertibleToBsonDocument, IEntity
    {
        public ObjectId id { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public DateTime validate { get; set; }
        public int quantity { get; set; }
        public double price { get; set; }
        public string picture { get; set; }
        public double totalPrice { get; set; }

        public  string getCollection()
        {
            return "itens";
        }

        public  ObjectId getId()
        {
            return id;
        }

        public BsonDocument ToBsonDocument()
        {
            throw new NotImplementedException();
        }
    }
}
