﻿using MongoDB.Bson;

namespace Mini_Erp.Models
{
    public interface IModel
    {
        ObjectId id { get; set; }
    }
}
