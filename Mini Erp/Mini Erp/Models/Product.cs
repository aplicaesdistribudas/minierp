﻿using Mini_Erp.DAO;
using MongoDB.Bson;
using System;

namespace Mini_Erp.Models
{
    public class Product : IConvertibleToBsonDocument, IEntity
    {
        public ObjectId id { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public DateTime validate { get; set; }
        public int quantity { get; set; }
        public double price { get; set; }
        public string picture { get; set; }
        public double totalPrice { get; set; }

        public string getCollection()
        {
            return "products";
        }

        public ObjectId getId()
        {
            return id;
        }

        public BsonDocument ToBsonDocument()
        {
            return new BsonDocument {
                { "_id", id },
                { "description", description },
                { "validate", validate },
                { "price", price },
                { "code", code },
                { "picture", picture }
                };
        }
    }
}
