﻿using Mini_Erp.DAO;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mini_Erp.Models
{
     class Cashier : IConvertibleToBsonDocument, IEntity
    {
        public ObjectId id { get; set; }
        public ObjectId user_id { get; set; }
        public double openingValue { get; set; }
        public double shutdownValue { get; set; }
        public DateTime openingDateTime { get; set; }
        public DateTime shutdownDateTime { get; set; }
        public int productsQuatitySelledInDay { get; set; }
        public int valueSelledInDay { get; set; }


        public BsonDocument ToBsonDocument()
        {
            return new BsonDocument {
                { "_id", id },
                { "user_id", user_id },
                { "openingValue", openingValue },
                { "shutdownValue", shutdownValue },
                { "openingDateTime", openingDateTime },
                { "shutdownDateTime", shutdownDateTime},
                { "productsQuatitySelledInDay", productsQuatitySelledInDay},
                { "valueSelledInDay",valueSelledInDay }
                };
        }

        public string getCollection()
        {
            return "cashiers";
        }

        public ObjectId getId()
        {
            return id;
        }
    }
}
