﻿using Mini_Erp.Classes;
using System;
using System.Windows.Forms;

namespace Mini_Erp
{
    public partial class AbrirCaixa : Form
    {
        public AbrirCaixa()
        {
            InitializeComponent();
        }

        private void abrirCaixaButton_Click(object sender, EventArgs e)
        {
            if (valorAberturaTextBox.Text.Length == 0)
            {
                MessageBox.Show("Insira um valor!!!");
                return;
            }

            double valor = Convert.ToDouble(valorAberturaTextBox.Text);
            StaticCashier.open(valor);

            Venda caixa = new Venda();
            caixa.Show();
            Dispose();
        }
    }
}
