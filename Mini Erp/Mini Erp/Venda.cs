﻿using Mini_Erp.Classes;
using Mini_Erp.DAO;
using Mini_Erp.Models;
using MongoDB.Driver;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Mini_Erp
{
    public partial class Venda : Form
    {

        private static Sell venda;
        private static double totalVenda;

        public Venda()
        {
            InitializeComponent();
            operadorNameLabel.Text += " " + Authentification.name;
            totalVenda = 0;
            this.newVenda();
        }



        private void addProdutoButton_Click(object sender, EventArgs e)
        {

            string codProduto = codigoProdutoTextBox.Text;
            int quantidade = int.Parse(quantidadeTextBox.Text);
            ProductRepository repo = new ProductRepository();
            var filterBuilder = Builders<Product>.Filter;
            var filter = filterBuilder.Eq("code", codProduto);

            Product product = repo.findOne(filter);

            foreach (var productVenda in venda.products)
            {
                if (productVenda.code.Equals(codProduto))
                {
                    quantidade += productVenda.quantity;

                    if (product.quantity < quantidade)
                    {
                        MessageBox.Show("Produto fora de estoque ou insuficiente. Quantidade restante: " + product.quantity);
                        return;
                    }
                    venda.products.Remove(productVenda);
                    totalVenda -= (productVenda.price * productVenda.quantity);
                    for (var i = 0; i < listaProdutosDataGridView.Rows.Count; i++)
                    {
                        if (listaProdutosDataGridView.Rows[i].Cells[0].Value.ToString().Equals(productVenda.code.ToString()))
                        {
                            listaProdutosDataGridView.Rows.RemoveAt(i);
                            break;
                        }
                    }
                    break;
                }
            }

            if (product == null)
            {
                MessageBox.Show("Produto não encontrado.");
                return;
            }

            if (DateTime.Compare(product.validate, DateTime.Now) < 0)
            {
                MessageBox.Show("Produto não encontrado.");
                return;
            }

            if (product.quantity == 0 || product.quantity < quantidade)
            {
                MessageBox.Show("Produto fora de estoque ou insuficiente. Quantidade restante: " + product.quantity);
                return;
            }


            double productTotalValor = (quantidade * product.price);
            totalVenda += productTotalValor;

            produtoImagemPictureBox.Image = null;

            if (product.picture != null)
            {
                Image image = Image.FromFile(product.picture);
                produtoImagemPictureBox.Image = image;
                produtoImagemPictureBox.SizeMode = PictureBoxSizeMode.CenterImage;
                produtoImagemPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            }

            listaProdutosDataGridView.Rows.Add(product.code, product.description, quantidade, product.price, productTotalValor);

            valorTotalDinamicoLabel.Text = Convert.ToString(totalVenda);

            product.quantity = quantidade;

            venda.products.Add(product);
        }

        private void voltarButton_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void Venda_Load(object sender, EventArgs e)
        {


        }

        private void newVenda()
        {
            venda = new Sell();
            venda.seller = Authentification.id;
            venda.created_at = DateTime.Now;
        }

        private void fecharCaixaButton_Click(object sender, EventArgs e)
        {
            StaticCashier.isColosed = true;
            FecharCaixa fechado = new FecharCaixa();
            StaticCashier.close();
            fechado.Show();
            Dispose();
        }

        private void fecharCompraButton_Click(object sender, EventArgs e)
        {
            StaticCashier.venda += 1;
            StaticCashier.valueSelledInDay += totalVenda;

            ProductRepository repoProdutos = new ProductRepository();

            Product produtoRepo = new Product();
            var filterBuilder = Builders<Product>.Filter;
            var filter = filterBuilder.Eq("code", "");


            foreach (var productVenda in venda.products)
            {
                filter = filterBuilder.Eq("code", productVenda.code);

                produtoRepo = repoProdutos.findOne(filter);

                produtoRepo.quantity -= productVenda.quantity;

                repoProdutos.Update(produtoRepo);
                StaticCashier.productsQuatitySelledInDay += productVenda.quantity;
            }

            SellRepository repo = new SellRepository();
            repo.Add(venda);

            StaticCashier.shutdownValue += totalVenda;
            totalVenda = 0;

            listaProdutosDataGridView.Rows.Clear();

            codigoProdutoTextBox.Text = string.Empty;
            quantidadeTextBox.Text = string.Empty;

            MessageBox.Show("Fechando compra:\n Valor Total: R$ " + valorTotalDinamicoLabel.Text + ". \n Obrigado pela preferência e volte sempre!", "Fechar comprar");
            valorTotalDinamicoLabel.Text = "0.00";

            this.newVenda();
        }
    }
}
