﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using Mini_Erp.Models;
using Mini_Erp.Classes;

namespace Mini_Erp
{
    public partial class Login : Form
    {

        public Login()
        {
            InitializeComponent();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            var users = MongoConnection.getDatabase().GetCollection<User>("users");
            var filterBuilder = Builders<User>.Filter;
            var filter = (filterBuilder.Eq("nickname", emailTextBox.Text) | filterBuilder.Eq("email", emailTextBox.Text)) & filterBuilder.Eq("password", senhaTextBox.Text);
            

            try
            {
                users.Find(filter).First();
            }
            catch (Exception)
            {
                MessageBox.Show("Senha ou usuário incorretos");
                return;
            }

            var user = users.Find(filter).First(); 

            Authentification.login(user.id, user.name, user.password, user.role);
            MessageBox.Show("Bem vindo: " + user.name);

            Index index = new Index();
            index.Show();
            Hide();
        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
