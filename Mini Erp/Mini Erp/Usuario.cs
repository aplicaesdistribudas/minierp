﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mini_Erp
{
    public partial class Usuario : Form
    {
        public Usuario()
        {
            InitializeComponent();
        }

        private void alterUserButton_Click(object sender, EventArgs e)
        {
            SelecaoUsuario selecaoUsuario = new SelecaoUsuario();
            selecaoUsuario.Show();
            Dispose();
        }

        private void Usuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            Index index = new Index();
            index.Show();
        }

        private void registerUserButton_Click(object sender, EventArgs e)
        {
            CadastroAlteracaoUsuario cadastroAlteracaoUsuario = new CadastroAlteracaoUsuario();
            cadastroAlteracaoUsuario.Show();
            Dispose();
        }
    }
}
